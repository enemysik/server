#include <boost/program_options.hpp>
namespace po = boost::program_options;
#include <fstream>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h> //errno
#include <string.h> //memset
#include <netdb.h> //struct
#include <unistd.h> //select
#include <fcntl.h> //fcntf();
#include <set> //set
using namespace std;

int Listen;

char ans[] = "Central Server";
int tryToSend, tryToGet, buffersize;
int tmp;

string myPort, config_file = "CS.cfg";

int SendMsg(int sock, char msg[])
{
    int res;
    for (int i = 0; i <= tryToSend; i++)
    {
        res = send(sock, msg, strlen(msg) + 1, 0);
        if (res < 0)
        {
            if (i < tryToSend)
            {
                continue;
            }
            else
            {
                cout << "Error: can not send to CCS\n";
                return -1;
            }
        }
        else
        {
            return res;
        }
    }
    return 0;
}

int GetMsg(int sock, char* buf)
{
    int res;
    for (int i = 0; i <= tryToGet; i++)
    {
        res = recv(sock, buf, buffersize, 0);
        if (res == 0)
        {
            cout << "Error: CCS disconnected\n";
            return -2;
        }
        if (res < 0)
        {
            if (i < tryToGet)
            {
                continue;
            }
            cout << "Error: can not send message\n";
            return -1;
        }
        return res;
    }
    return 0;
}

int main(int ac, char* av[])
{
    po::options_description config("Config");
    config.add_options()
        ("myPort", po::value<string>(&myPort)->default_value("9999"), "My port")
        ("trytosend", po::value<int>(&tryToSend)->default_value(5), "try to send")
        ("trytoget", po::value<int>(&tryToGet)->default_value(5), "try to get")
        ("buffersize", po::value<int>(&buffersize)->default_value(1024), "buffer size")
        ;

    po::options_description cmdline_options;
    cmdline_options.add(config);

    po::options_description config_file_options;
    config_file_options.add(config);

    po::variables_map vm;
    po::store(po::command_line_parser(ac, av).options(cmdline_options).run(), vm);
    po::notify(vm);

    ifstream ifs(config_file.c_str());
    if (!ifs)
    {
        cout << "Error: can not open config file: " << config_file << "\n";
    }
    else
    {
        po::store(po::parse_config_file(ifs, config_file_options), vm);
        po::notify(vm);
    }

    struct addrinfo hints, *res;
    memset(&hints, 0, sizeof(hints));

    //hints.ai_family = AF_INET;
    //hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    //hints.ai_protocol = IPPROTO_TCP;

    if(getaddrinfo(0, myPort.c_str(), &hints, &res) < 0)
    {
        cout << "Error: getaddrinfo\n";
        return -1;
    }

    Listen = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (Listen < 0)
    {
        cout << "Error: socket\n";
        return -1;
    }

    fcntl(Listen, F_SETFL, O_NONBLOCK);

    if (bind(Listen, res->ai_addr, res->ai_addrlen) < 0)
    {
        cout << "Error: bind\n" << errno << "\n";
        shutdown(Listen, 2);
        return -1;
    }

    freeaddrinfo(res);

    listen(Listen, 2);
    set<int> clients;
   // clients.clear();

    cout << "Start server\n";

    for (;;)
    {
        fd_set readset;
        FD_ZERO(&readset);
        FD_SET(Listen, &readset);

        for(auto it = clients.begin(); it !=clients.end(); it++)
            FD_SET(*it, &readset);

        timeval timeout;
        timeout.tv_sec = 15;
        timeout.tv_usec = 0;

        int mx = max(Listen, *max_element(clients.begin(), clients.end()));
        if(select(mx + 1, &readset, NULL, NULL, &timeout) < 0)
        {
            cout << "Error: select\n";
            shutdown(Listen, 2);
            return 1;
        }

        if(FD_ISSET(Listen, &readset))
        {
            int Connect = accept(Listen, NULL, NULL);
            if(Connect < 0)
            {
                cout << "Error: accept\n";
                continue;
            }

            fcntl(Connect, F_SETFL, O_NONBLOCK);

            cout << "CCS connected\n";

            clients.insert(Connect);
        }

        for(auto it = clients.begin(); it != clients.end(); it++)
        {
            if(FD_ISSET(*it, &readset))
            {

                char* buf = new char[1024];
                memset(buf, 0, 1024);
                tmp = GetMsg(*it, buf);
                if (tmp < 0)
                {
                    cout << "Error: CCS disconected\n";
                    close(*it);
                    clients.erase(*it);
                    continue;
                }
                else
                {
                    cout << "We got: " << buf << "\n";
                    if (SendMsg(*it, ans) < 0)
                    {
                        cout << "Error: can not send message\n";
                        close(*it);
                        clients.erase(*it);
                        continue;
                    }
                    cout << "We send answer\n";
                }
                delete[] buf;
            }
        }
    }
}
