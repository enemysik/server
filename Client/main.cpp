#include <boost/program_options.hpp>
namespace po = boost::program_options;
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h> //errno
#include <string.h> //memset
#include <netdb.h> //struct
using namespace std;


int Connect;

char message1[] = "CS SELECT", message2[] = "CCS SELECT", buf[1024], SE[] = "Server error";
int tryToConnect, tryToSend, tryToGet, buffersize;
string serverIP, serverPort, config_file = "Client.cfg";
int choice;

int ConnectToServer(int sock, struct addrinfo* res, int count)
{
    sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sock < 0)
    {
        cout << "Error: socket\n";
        return -1;
    }
    for (int i = 0; i <= count; i++)
    {
        if (connect(sock, res->ai_addr, res->ai_addrlen) < 0)
        {
            if (i < count)
            {
                continue;
            }
            else
            {
                cout << "Can not connect to server\n";
                return -1;
            }
        }
        else
        {
            return sock;
        }
    }
    return -1;
}
int SendMsg(int sock, char msg[], int count)
{
    int res;
    for (int i = 0; i <= count; i++)
    {
        res = send(sock, msg, strlen(msg) + 1, 0);
        if (res < 0)
        {
            if (i < count)
            {
                continue;
            }
            else
            {
                cout << "Can not send to server\n";
                return -1;
            }
        }
        else
        {
            return res;
        }
    }
    return -1;
}
int GetMsg(int sock, char* buf, int count)
{
    int res;
    for (int i = 0; i <= count; i++)
    {
        res = recv(sock, buf, buffersize, 0);
        if (res == 0)
        {
            cout << "Server error1\n";
            return -1;
        }
        if (res < 0)
        {
            if (i < count)
            {
                continue;
            }
            cout << "Server error2\n";
            return -1;
        }
        return res;
    }
    return -1;
}

int main(int ac, char* av[])
{

    po::options_description config("Config");
    config.add_options()
        ("serverIP", po::value<string>(&serverIP)->default_value("127.0.0.1"), "Server IP")
        ("serverport", po::value<string>(&serverPort)->default_value("8888"), "Server port")
        ("trytosend", po::value<int>(&tryToSend)->default_value(5), "try to send")
        ("trytoconnect", po::value<int>(&tryToConnect)->default_value(5), "try to connect")
        ("trytoget", po::value<int>(&tryToGet)->default_value(5), "try to get")
        ("buffersize", po::value<int>(&buffersize)->default_value(1024), "buffer size")
        ;

    po::options_description cmdline_options;
    cmdline_options.add(config);

    po::options_description config_file_options;
    config_file_options.add(config);

    po::variables_map vm;
    po::store(po::command_line_parser(ac, av).options(cmdline_options).run(), vm);
    po::notify(vm);

    ifstream ifs(config_file.c_str());
    if (!ifs)
    {
        cout << "can not open config file: " << config_file << "\n";
    }
    else
    {
        po::store(po::parse_config_file(ifs, config_file_options), vm);
        po::notify(vm);
    }

    struct addrinfo hints, *res;

    memset(&hints, 0, sizeof(hints));

    hints.ai_family = AF_INET;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_socktype = SOCK_STREAM;

    if(getaddrinfo(serverIP.c_str(), serverPort.c_str(), &hints, &res) < 0)
    {
        cout << "Error: getaddrinfo\n";
        return -1;
    }

    for (;;)
    {
        cout <<"Write 1 or 2: ";
        cin >> choice;
        switch (choice)
        {
        case 1:
            Connect = ConnectToServer(Connect, res, tryToConnect);
            if (Connect == -1)
            {
                continue;
            }
            if (SendMsg(Connect, message1, tryToSend) == -1)
            {
                cout << "Server error3\n";
                continue;
            }
            break;
        case 2:
            Connect = ConnectToServer(Connect, res, tryToConnect);
            if (Connect == -1)
            {
                continue;
            }
            if (SendMsg(Connect, message2, tryToSend) == -1)
            {
                cout << "Server error4\n";
                continue;
            }
            break;
        default:
            cout << "It isn't 1 or 2\n";
            continue;
        }
        memset(&buf, 0, 1024);
        GetMsg(Connect, buf, tryToGet);
        shutdown(Connect, 2);
        if (strncmp(buf, SE, strlen(SE)) == 0)
        {
            cout << "Server error5\n";
            continue;
        }
        cout << "We got: " << buf << "\n";
    }
    return 0;
}
