#include <boost/program_options.hpp>
namespace po = boost::program_options;
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h> //errno
#include <string.h> //memset
#include <netdb.h> //struct
using namespace std;

int Listen, Connect, CS;
int bytes_read;
char tmp[] = "CS";
int tryToConnect = 5;
int tryToSend = 5;
int tryToGet = 5;
int buffersize = 1024;
int maxListen = SOMAXCONN;
char SE[] = "Server error";
string CSIP, CSPort, myPort, config_file = "CCS.cfg";

int ConnectToServer(int sock, struct addrinfo* res, int count)
{
    sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sock < 0)
    {
        cout << "Error: socket\n";
        return -1;
    }
    int tmp;
    for (int i = 0; i <= count; i++)
    {
        tmp = connect(sock, res->ai_addr, res->ai_addrlen);
        if (tmp < 0)
        {
            if (i < count)
            {
                continue;
            }
            else
            {
                cout << "Error: Can not connect to CS\n";
                return -1;
            }
        }
        else
        {
            break;
        }
    }
    return sock;
}
int SendMsg(int sock, char msg[], int count, int br = -1)
{
    int res;
    if (br == -1)
    {
        br = strlen(msg);
    }
    for (int i = 0; i <= count; i++)
    {
        res = send(sock, msg, br, 0);
        if (res < 0)
        {
            if (i < count)
            {
                continue;
            }
            else
            {
                return -1;
            }
        }
        else
        {
            return res;
        }
    }
    return 0;
}
int GetMsg(int sock, char* buf, int count)
{
    int res;
    for (int i = 0; i <= count; i++)
    {
        res = recv(sock, buf, buffersize, 0);
        if (res == 0)
        {
            return -2;
        }
        if (res < 0)
        {
            if (i < count)
            {
                continue;
            }
            return -1;
        }
        return res;
    }
    return 0;
}


int main(int ac, char* av[])
{

    po::options_description config("Config");
    config.add_options()
        ("CSIP", po::value<string>(&CSIP)->default_value("127.0.0.1"), "Server IP")
        ("myPort", po::value<string>(&myPort)->default_value("8888"), "My Port")
        ("CSPort", po::value<string>(&CSPort)->default_value("9998"), "CS Port")
        ("trytosend", po::value<int>(&tryToSend)->default_value(5), "try to send")
        ("trytoconnect", po::value<int>(&tryToConnect)->default_value(5), "try to connect")
        ("trytoget", po::value<int>(&tryToGet)->default_value(5), "try to get")
        ("buffersize", po::value<int>(&buffersize)->default_value(1024), "buffer size")
        ;

    po::options_description cmdline_options;
    cmdline_options.add(config);

    po::options_description config_file_options;
    config_file_options.add(config);

    po::variables_map vm;
    po::store(po::command_line_parser(ac, av).options(cmdline_options).run(), vm);
    po::notify(vm);

    ifstream ifs(config_file.c_str());
    if (!ifs)
    {
        cout << "can not open config file: " << config_file << "\n";
    }
    else
    {
        po::store(po::parse_config_file(ifs, config_file_options), vm);
        po::notify(vm);
    }

    struct addrinfo hints, *res;
    memset(&hints, 0, sizeof(hints));

    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_protocol = IPPROTO_TCP;

    getaddrinfo(0, myPort.c_str(), &hints, &res);

    shutdown(Listen, 2);

    Listen = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (Listen < 0)
    {
        cout << "Error: socket\n";
        return -1;
    }
    cout << "socket\n";

    const int on = 1;
    setsockopt(Listen, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
    if (bind(Listen, res->ai_addr, res->ai_addrlen) < 0)
    {
        cout << "Error: bind\n";
        return -1;
    }
    cout << "bind\n";

    getaddrinfo(CSIP.c_str(), CSPort.c_str(), &hints, &res);
    CS = ConnectToServer(CS, res, tryToConnect);
    listen(Listen, maxListen);

    cout << "Start server\n";

    for (;;)
    {
        Connect = accept(Listen, NULL, NULL);
        if(Connect)
        {
            cout << "Cliet connected: " << Connect << "\n";
            char* buf = new char[1024];
            memset(buf, 0, sizeof(buf));
            bytes_read = GetMsg(Connect, buf, tryToGet);
            if (bytes_read < 0)
            {
                if (bytes_read == -2)
                {
                    cout << "Error: Client disconnected\n";
                }
                else
                {
                    cout << "Error: Client do not send message\n";
                }
                continue;
            }
            if (bytes_read > 0)
            {
                cout << "We got: " << buf << "\n";
                if (strncmp(buf, tmp, strlen(tmp)) == 0)
                {
                    if (CS == -1)
                    {
                        CS = ConnectToServer(CS, res, tryToConnect);
                        if (CS == -1)
                        {
                           SendMsg(Connect, SE, tryToSend);
                           continue;
                        }

                    }
                    if (SendMsg(CS, buf, tryToSend, bytes_read) < 0)
                    {
                        cout << "Error: can not send to CS\n";
                        CS = -1;
                        SendMsg(Connect, SE, tryToSend);
                        continue;
                    }
                    bytes_read = GetMsg(CS, buf, tryToGet);
                    if (bytes_read < 0)
                    {
                        if (bytes_read == -2)
                        {
                            cout << "Error: CS disconnected\n";
                            CS = -1;
                        }
                        else
                        {
                            cout << "Error: CS do not send message";
                        }
                        SendMsg(Connect, SE, tryToSend);
                        continue;
                    }
                }
                else
                {
                    buf = "Connect to Central Server";
                }
                if (SendMsg(Connect, buf, tryToSend) < 0)
                {
                    cout << "Error: can not send to Client\n";
                    continue;
                }
                cout << "We send answer: " << buf << "\n";
                //buf = nullptr;
               // delete buf;
            }
            shutdown(Connect, 2);
        }
    }
}
