QT += core
QT -= gui

CONFIG += c++11

TARGET = CCS
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

LIBS += -L/usr/lib -lboost_program_options
